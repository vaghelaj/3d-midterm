#include "PathFollowing.h"

#include <iostream>
#include <math.h>
#include <ctime> 
#include <cstdlib>

using namespace std;

struct Coordinates {
	float x = 0;
	float y = 0;
};

vector<Coordinates> SplinePath;

PathFollowing::PathFollowing() 
{
	vector<float> vValue;
	int v = 26;
	float increment = (float)1 / (float)v;
	float value = 0.0f;

	for (int i = 0; i < v; i++)
	{
		value += increment;
		vValue.push_back(value);
	}

	//BEGINNING of curve.
	float t[11] = { 0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1 };

	//Support point 1
	int point1Control_x = 0;
	int point1Control_y = 0;

	//Point 1
	int point1_x = -2;
	int point1_y = 5;

	//Support point 2
	int point2Control_x = 4;
	int point2Control_y = 0;

	//Point 2
	int point2_x = 4;
	int point2_y = -5;

	//Support point 3
	int point3Control_x = 7;
	int point3Control_y = 0;

	//Point 3
	int point3_x = 7;
	int point3_y = -1;

	//Support point 4
	int point4Control_x = 9;
	int point4Control_y = 0;

	//Point 4
	int point4_x = 6;
	int point4_y = 6;

	float p_x1, p_x2, p_y1, p_y2, p_x3, p_y3, p_x4, p_y4, t_x, t_y;


	vector<Coordinates> SplinePath1;
	vector<Coordinates> SplinePath2;
	vector<Coordinates> SplinePath3;
	vector<Coordinates> SplinePath4;

	for (int i = 0; i < vValue.size(); i++)
	{
		p_x1 = 0.5f*((point1_x*2.0f) + (-point1Control_x + point2_x)*vValue[i] + ((point1Control_x*2.0f) - (point1_x*5.0f) + (point2_x*4.0f) - point2Control_x)*(pow(vValue[i], 2)) + (-point1Control_x + (point1_x*3.0f) - (point2_x*3.0f) + point2Control_x)*(pow(vValue[i], 3)));
		p_y1 = 0.5f*((point1_y*2.0f) + (-point1Control_y + point2_y)*vValue[i] + ((point1Control_y*2.0f) - (point1_y*5.0f) + (point2_y*4.0f) - point2Control_y)*(pow(vValue[i], 2)) + (-point1Control_y + (point1_y*3.0f) - (point2_y*3.0f) + point2Control_y)*(pow(vValue[i], 3)));

		SplinePath1.push_back({ p_x1, p_y1 });

		p_x2 = 0.5f*((point2_x*2.0f) + (-point2Control_x + point3_x)*vValue[i] + ((point2Control_x*2.0f) - (point2_x*5.0f) + (point3_x*4.0f) - point3Control_x)*(pow(vValue[i], 2)) + (-point2Control_x + (point2_x*3.0f) - (point3_x*3.0f) + point3Control_x)*(pow(vValue[i], 3)));
		p_y2 = 0.5f*((point2_y*2.0f) + (-point2Control_y + point3_y)*vValue[i] + ((point2Control_y*2.0f) - (point2_y*5.0f) + (point3_y*4.0f) - point3Control_y)*(pow(vValue[i], 2)) + (-point2Control_y + (point2_y*3.0f) - (point3_y*3.0f) + point3Control_y)*(pow(vValue[i], 3)));

		SplinePath2.push_back({ p_x2, p_y2 });

		p_x3 = 0.5f*((point3_x*2.0f) + (-point3Control_x + point4_x)*vValue[i] + ((point3Control_x*2.0f) - (point3_x*5.0f) + (point4_x*4.0f) - point4Control_x)*(pow(vValue[i], 2)) + (-point3Control_x + (point3_x*3.0f) - (point4_x*3.0f) + point4Control_x)*(pow(vValue[i], 3)));
		p_y3 = 0.5f*((point3_y*2.0f) + (-point3Control_y + point4_y)*vValue[i] + ((point3Control_y*2.0f) - (point3_y*5.0f) + (point4_y*4.0f) - point4Control_y)*(pow(vValue[i], 2)) + (-point3Control_y + (point3_y*3.0f) - (point4_y*3.0f) + point4Control_y)*(pow(vValue[i], 3)));

		SplinePath3.push_back({ p_x3, p_y3 });

		p_x4 = 0.5f*((point4_x*2.0f) + (-point4Control_x + point1_x)*vValue[i] + ((point4Control_x*2.0f) - (point4_x*5.0f) + (point1_x*4.0f) - point1Control_x)*(pow(vValue[i], 2)) + (-point4Control_x + (point4_x*3.0f) - (point1_x*3.0f) + point1Control_x)*(pow(vValue[i], 3)));
		p_y4 = 0.5f*((point4_y*2.0f) + (-point4Control_y + point1_y)*vValue[i] + ((point4Control_y*2.0f) - (point4_y*5.0f) + (point1_y*4.0f) - point1Control_y)*(pow(vValue[i], 2)) + (-point4Control_y + (point4_y*3.0f) - (point1_y*3.0f) + point1Control_y)*(pow(vValue[i], 3)));

		SplinePath4.push_back({ p_x4, p_y4 });

	
	}
	//ENDING of curve code.

	//Concatenate SplinePath
	SplinePath.insert(SplinePath.begin(), SplinePath1.begin(), SplinePath1.end());
	SplinePath.insert(SplinePath.end(), SplinePath2.begin(), SplinePath2.end());
	SplinePath.insert(SplinePath.end(), SplinePath3.begin(), SplinePath3.end());
	SplinePath.insert(SplinePath.end(), SplinePath4.begin(), SplinePath4.end());
}

PathFollowing::~PathFollowing()
{
}

bool PathFollowing::initialize(int w, int h)
{
    // set background color
    glClearColor(0.1f, 0.1f, 0.1f, 1.0f);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    updateProjection();

    return true;
}

void PathFollowing::updateProjection()
{
    // get window dimensions
    int w = getWindow()->getWidth();
    int h = getWindow()->getHeight();

    // window aspect ratio
    float aspectRatio = w / (float)h;

    // dimensions of viewable area
    mViewHeight = 15;
    mViewWidth = mViewHeight * aspectRatio;

    // bounds of viewable area
    mViewLeft = -0.5f * mViewWidth;
    mViewRight = mViewLeft + mViewWidth;
    mViewBottom = -0.5f * mViewHeight;
    mViewTop = mViewBottom + mViewHeight;

    // set OpenGL projection transformation
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(mViewLeft, mViewRight, mViewBottom, mViewTop, -1.0, 1.0);
}

glm::vec2 PathFollowing::screenToWorld(int scrX, int scrY) const
{
    int scrWidth = getWindow()->getWidth();
    int scrHeight = getWindow()->getHeight();

    scrY = getWindow()->getHeight() - scrY - 1;    // flip y

    float worldX = mViewLeft + (scrX / (float)scrWidth) * mViewWidth;
    float worldY = mViewBottom + (scrY / (float)scrHeight) * mViewHeight;

    return glm::vec2(worldX, worldY);
}

void PathFollowing::shutdown()
{
	exit;
}

void PathFollowing::resize(int w, int h)
{
    // update OpenGL viewport
    glViewport(0, 0, w, h);

    updateProjection();
}

void PathFollowing::setTransform(const Transform& T)
{
    glLoadIdentity();
    glTranslatef(T.mPos.x, T.mPos.y, 0.0f);
    glRotatef(T.mAngle, 0.0f, 0.0f, 1.0f);
    glScalef(T.mScale.x, T.mScale.y, 1.0f);
}

void PathFollowing::drawGrid()
{
    glBegin(GL_LINES);
    glColor4f(1.0f, 1.0f, 1.0f, 0.15f);
    for (float x = 0.0f; x <= mViewRight; x += 1.0f) {
        glVertex2f(x, mViewBottom);
        glVertex2f(x, mViewTop);
    }
    for (float x = 0.0f; x >= mViewLeft; x -= 1.0f) {
        glVertex2f(x, mViewBottom);
        glVertex2f(x, mViewTop);
    }
    for (float y = 0.0f; y <= mViewTop; y += 1.0f) {
        glVertex2f(mViewLeft, y);
        glVertex2f(mViewRight, y);
    }
    for (float y = 0.0f; y >= mViewBottom; y -= 1.0f) {
        glVertex2f(mViewLeft, y);
        glVertex2f(mViewRight, y);
    }
    glEnd();
}

float xT = 6.0f;
float yT = 6.0f;
int pathCoord = 0;

void PathFollowing::draw()
{
    // clear the screen
    glClear(GL_COLOR_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);

    glLoadIdentity();
    drawGrid();

	/*int num [20];
	int i = 0;
	srand((unsigned)time(0));
	int random_integer;
	int lowest = 0, highest = 20;
	int range = (highest - lowest) + 1;
	for (int index = 0; index < 20; index++) {
		random_integer = (lowest + int(range*rand() / (RAND_MAX + 1.0))) - 10;
		num[i] = random_integer;
		i += 1;
	}*/
    // ...draw stuff...

	
	for (int i = 0; i < SplinePath.size(); i++)
	{
		glLoadIdentity(); //Reset Coordinate
		glPointSize(10.0f);
		glBegin(GL_POINTS);
		glColor3f(1.0f, 1.0f, 0.0f);
		glVertex2f(SplinePath[i].x, SplinePath[i].y);
		glEnd();
	}

	glLoadIdentity();
	//p_x1 += 0.1f;
	//p_y2 += 0.1f;
	xT += 0.1f;
	yT += 0.1f;
	glTranslatef(SplinePath[pathCoord].x, SplinePath[pathCoord].y, 0);
	if (pathCoord == SplinePath.size()-1)
	{
		pathCoord = 0;

	}
	pathCoord++;
	glBegin(GL_TRIANGLE_FAN);
	glColor3f(1.0f, 1.0f, 1.0f);
	int numSegments = 24;
	float r = 0.15f;
	float angleStep = 2 * 3.1415f / numSegments;
	glVertex2f(0, 0);
	for (int i = 0; i <= numSegments; i++) {
		float angle = i * angleStep;
		float x = r * cos(angle);
		float y = r * sin(angle);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex2f(x, y);
	}
	glEnd();
	glFlush();
	//shutdown();
}


void PathFollowing::update(float dt)
{
    // get pointer to keyboard data
    const glsh::Keyboard* kb = getKeyboard();

    // quit if Escape key was pressed
    if (kb->keyPressed(glsh::KC_ESCAPE)) {
        quit();
    }

    // handle mouse click
    const glsh::Mouse* mouse = getMouse();
    if (mouse->buttonPressed(glsh::MOUSE_BUTTON_LEFT)) {
        int scrX = mouse->getX();
        int scrY = mouse->getY();

        // ...
    }
    
    // ...update ship, etc...
}