#ifndef GAME_H_
#define GAME_H_

#include "GLSH.h"

struct Transform
{
    glm::vec2               mPos                        = { 0.0f, 0.0f };
    glm::vec2               mScale                      = { 1.0f, 1.0f };
    float                   mAngle                      = 0.0f;             // in degrees
};


class ItemCollecting : public glsh::App
{
    // 2d projection stuff
    float                   mViewWidth                  = 0.0f;
    float                   mViewHeight                 = 0.0f;
    float                   mViewLeft                   = 0.0f;
    float                   mViewRight                  = 0.0f;
    float                   mViewBottom                 = 0.0f;
    float                   mViewTop                    = 0.0f;

    void                    updateProjection();
    void                    setTransform(const Transform& T);

    void                    drawGrid();

    glm::vec2               screenToWorld(int scrX, int scrY) const;

public:
                            ItemCollecting();
                            ~ItemCollecting();

    bool                    initialize(int w, int h)    override;
    void                    shutdown()                  override;
    void                    resize(int w, int h)        override;
    void                    draw()                      override;
    void                    update(float dt)            override;
};

#endif
