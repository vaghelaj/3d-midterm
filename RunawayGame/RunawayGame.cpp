#include "RunawayGame.h"

#include <iostream>

RunawayGame::RunawayGame() 
{
}

RunawayGame::~RunawayGame()
{
}

bool RunawayGame::initialize(int w, int h)
{
    // set background color
    glClearColor(0.1f, 0.1f, 0.1f, 1.0f);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    updateProjection();

    return true;
}

void RunawayGame::updateProjection()
{
    // get window dimensions
    int w = getWindow()->getWidth();
    int h = getWindow()->getHeight();

    // window aspect ratio
    float aspectRatio = w / (float)h;

    // dimensions of viewable area
    mViewHeight = 15;
    mViewWidth = mViewHeight * aspectRatio;

    // bounds of viewable area
    mViewLeft = -0.5f * mViewWidth;
    mViewRight = mViewLeft + mViewWidth;
    mViewBottom = -0.5f * mViewHeight;
    mViewTop = mViewBottom + mViewHeight;

    // set OpenGL projection transformation
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(mViewLeft, mViewRight, mViewBottom, mViewTop, -1.0, 1.0);
}

glm::vec2 RunawayGame::screenToWorld(int scrX, int scrY) const
{
    int scrWidth = getWindow()->getWidth();
    int scrHeight = getWindow()->getHeight();

    scrY = getWindow()->getHeight() - scrY - 1;    // flip y

    float worldX = mViewLeft + (scrX / (float)scrWidth) * mViewWidth;
    float worldY = mViewBottom + (scrY / (float)scrHeight) * mViewHeight;

    return glm::vec2(worldX, worldY);
}

void RunawayGame::shutdown()
{
}

void RunawayGame::resize(int w, int h)
{
    // update OpenGL viewport
    glViewport(0, 0, w, h);

    updateProjection();
}

void RunawayGame::setTransform(const Transform& T)
{
    glLoadIdentity();
    glTranslatef(T.mPos.x, T.mPos.y, 0.0f);
    glRotatef(T.mAngle, 0.0f, 0.0f, 1.0f);
    glScalef(T.mScale.x, T.mScale.y, 1.0f);
}

void RunawayGame::drawGrid()
{
    glBegin(GL_LINES);
    glColor4f(1.0f, 1.0f, 1.0f, 0.15f);
    for (float x = 0.0f; x <= mViewRight; x += 1.0f) {
        glVertex2f(x, mViewBottom);
        glVertex2f(x, mViewTop);
    }
    for (float x = 0.0f; x >= mViewLeft; x -= 1.0f) {
        glVertex2f(x, mViewBottom);
        glVertex2f(x, mViewTop);
    }
    for (float y = 0.0f; y <= mViewTop; y += 1.0f) {
        glVertex2f(mViewLeft, y);
        glVertex2f(mViewRight, y);
    }
    for (float y = 0.0f; y >= mViewBottom; y -= 1.0f) {
        glVertex2f(mViewLeft, y);
        glVertex2f(mViewRight, y);
    }
    glEnd();
}


void RunawayGame::draw()
{
    // clear the screen
    glClear(GL_COLOR_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);

    glLoadIdentity();
	drawGrid();

	
	for (int i = 0; i < 100; i++)
	{
		
	}
}

void RunawayGame::update(float dt)
{
    // get pointer to keyboard data
    const glsh::Keyboard* kb = getKeyboard();

    // quit if Escape key was pressed
    if (kb->keyPressed(glsh::KC_ESCAPE)) {
        quit();
    }

    // handle mouse click
    const glsh::Mouse* mouse = getMouse();
    if (mouse->buttonPressed(glsh::MOUSE_BUTTON_LEFT)) {
        int scrX = mouse->getX();
        int scrY = mouse->getY();

        // ...
    }
    
    // ...update ship, etc...
}
